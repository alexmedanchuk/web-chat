$(document).ready(function() {

	$("#search").keyup(function() {
		var channel = $("#channel").val();
    	var match = $("#search").val();
	    $.ajax({
	    url : "/search/" + channel,
	    type : "GET",
	    dataType: "html",
	    data : {
			'match' : match,
	    },
	    success : searchSuccess
	    });
	});
function searchSuccess(data, textStatus, jqXHR) {
    $(".search_output").html(data);
};

});