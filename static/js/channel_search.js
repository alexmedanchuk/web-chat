$(document).ready(function() {

	$("#search_box").keyup(function() {
    	var match = $("#search_box").val();
	    $.ajax({
	    url : "/find/channel",
	    type : "GET",
	    dataType: "html",
	    data : {
			'match' : match,
	    },
	    success : searchSuccess
	    });
	});
function searchSuccess(data, textStatus, jqXHR) {
    $(".search_output").html(data);
};

});