      $(document).ready(function () {

        document.getElementById('scroll').scrollTop = 9999;
        var ws = new WebSocket("ws://" + window.location.hostname + ":" + window.location.port + "/ws");
        ws.onopen = function(evt) { 
            var channel = $("#channel").val();
            var initial_obj = JSON.stringify({"initial":channel});
            ws.send(initial_obj)
        };
            
        ws.onclose = function(evt) {};
        ws.onmessage = function(evt) { $(".new_messages").append("<pre>" + evt.data + "</pre>"); 
                                      document.getElementById('scroll').scrollTop = 9999;
                                      };
 
       $("#send").click(function(evt) {
          evt.preventDefault();
          var date = new Date();
          var user = $("#user").val();
          var message = $("#message").val();
          var channel = $("#channel").val();
          var obj = JSON.stringify({
              "channel":channel,
              "user": user,
              "message": message,
              "date": date.toString()
              });          
          $.ajax({
                url : "/channels/" + channel,
                type : "POST",
                dataType: "json",
                data : {

                  'date' : date.toString(),
                  'user' : user,
                  'message' : message

                }
          });//ajax_end       
          ws.send(obj);
        }); 
      });
