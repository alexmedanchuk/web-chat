import os
import json
import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.httpserver

from urllib.parse import urlparse
from pymongo import MongoClient

# TCP port to listen.
PORT = os.getenv('PORT', 8888)

# Database connection URI
URI = os.getenv('MONGOLAB_URI', 'mongodb://localhost:27017/chat')
DATABASE = urlparse(URI).path[1:]


class BaseHandler(tornado.web.RequestHandler):
    
    """Base handler for all request handlers in this app.
    
    Overrides 'initialize' and 'get_current_user' methods of base class.
    """
    
    def initialize(self):
        """Initializes connection to mongoDB database."""
        connection = MongoClient(URI)
        self.db = connection[DATABASE]
        
    def get_current_user(self):
        """Returns current user."""
        return self.get_secure_cookie('user')
    
    
class LoginHandler(BaseHandler):
    
    """Login request handler.
    
    Overrides 'get' and 'post' methods of base class. Authenticates users. 
    """
    
    def get(self, errors=[]):
        """Returns login form."""
        if self.current_user:
            self.redirect('/')
        self.render('templates/login.html', errors=errors)
        
    def post(self):
        """Authenticates user if valid login form data."""
        if self.get_argument('name') and self.get_argument('password'):
            users = self.db.users
            user = users.find_one({'name':self.get_argument('name'), 
                                   'password':self.get_argument('password')})
            if user:
                self.set_secure_cookie('user', user['name'])
                self.redirect('/')
                return None
        # If errors in form data, returns form with errors.
        errors = ['Wrong password or username',]
        self.get(errors=errors)
            
class LogoutHandler(BaseHandler):
    
    """Logout request handler.
    
    Overrides 'get' method of base class. Logs out user and redirects him
    to logout page.
    """
    
    def get(self):
        """Clears cookie with current user and redirects to logout page."""
        self.clear_cookie('user')
        self.render('templates/logout.html')
        

class RegistrationHandler(BaseHandler):        
    
    """Registration request handler.
    
    Overrides 'get' and 'post' methods of base class. Registrates users
    in the database.
    """
    
    def get(self, errors=[]):
        """Returns registration form."""
        if self.current_user:
            self.redirect('/')
        self.render('templates/register.html', errors=errors)
        
    def post(self):
        """
        Registrates new user in the database if valid form data and redirects
        him to login page. Else returns registration form with errors.
        """
        errors = []
        users = self.db.users
        name = self.get_argument('username')
        password = self.get_argument('password')
        password2 = self.get_argument('password2')
        if not name or not password or not password2:
            errors.append('All fields are required!')
        if users.find_one({'name':name}):
            errors.append('This name is already taken!')
        if password != password2:
            errors.append('Password confirmation is not correct!')
        if errors:
            self.get(errors=errors)
            return None
        self.db.users.insert({'name':name, 'password':password})
        self.redirect('/register/success')

class RegisterSuccessHandler(BaseHandler):
    
    """Successfull registration request handler.
    
    Overrides 'get' method of base class. Notification of successfull 
    registration.
    """
    
    def get(self):
        """Returns page with successfull registration notification."""
        self.render('templates/register_success.html')
        
class MainHandler(BaseHandler):
    
    """Chat main page request handler.
    
    Overrides 'get' and 'post' methods of base class. Represents page 
    with all chat channels. Authetication required. 
    """
    
    @tornado.web.authenticated
    def get(self, errors=[]):
        """Returns chat main page with already created channels."""
        channels = self.db.channels.find()
        self.render('templates/index.html', 
                    user=self.current_user, 
                    channels=channels,
                    errors=errors)
        
    @tornado.web.authenticated   
    def post(self):
        """Adds new channel to chat."""
        errors = []
        channel = self.get_argument('channel')
        if not channel:
            errors.append('Input new channel name, please!')
        if self.db.channels.find_one({'name':channel}):
            errors.append('This channel is already exsist!')
        if not channel.isalnum():
            errors.append('Channel name must be only letters and numbers!')
        # If any errors returns main page with errors printed.
        if errors:
            self.get(errors=errors)
            return None
        self.db.channels.insert({'name':channel, 'messages':[]})
        self.get()
        
        
class ChannelHandler(BaseHandler): 
    
    """Chat channel request handler.
    
    Overrides 'get' and 'post' methods of base class. Represents one 
    chat channel. Authentication required.
    """
    
    @tornado.web.authenticated
    def get(self, channel):
        """
        Takes additional argument 'channel'(name of current channel) and 
        returns current channel page. Queries for old messages and 
        renders channel template with this messages.
        """ 
        channel_data = self.db.channels.find_one({'name':channel})
        messages = channel_data['messages']
        self.render('templates/channel.html', channel=channel, 
                    messages=messages, user=self.current_user)
        
    @tornado.web.authenticated
    def post(self, channel):
        """
        Takes additional argument 'channel'(name of current channel) and 
        writes new message on current channel to the database.
        """
        user = self.get_argument('user')
        message = self.get_argument('message')
        date = self.get_argument('date')
        self.db.channels.update({'name':channel}, 
                                {'$push':
                                    {
                                        'messages': {'message':message,
                                                     'author':user,
                                                     'date':date}
                                    }
                                })
        

class ChannelSearchHandler(BaseHandler):
    
    """Search for messages in current channel request handler.
    
    Overrides 'get' method of base class. Returns search result of
    messages in current channel. Autentication required.
    """
    
    @tornado.web.authenticated
    def get(self, channel):
        """
        Takes additional argument 'channel'(name of current channel) and 
        returns template rendered with messages which contain query 
        parameter 'match'. 
        """        
        search_results = []
        match = self.get_argument('match').lower()
        messages = self.db.channels.find_one({'name':channel})['messages']
        if match:
            for message in messages:
                if match in message['author'].lower() or match in message['date'].lower() or \
                   match in message['message'].lower():
                    search_results.append(message)
        self.render("templates/search_on_channel.html", messages=search_results)
        
class SearchChannelNameHandler(BaseHandler):
    
    """Channels search request handler.
    
    Overrides 'get' method of base class. Returns search result of
    channels in main page. Autentication required.
    """
    
    @tornado.web.authenticated
    def get(self):
        """
        Returns template rendered with channels which contain query 
        parameter 'match'.
        """
        search_results = []
        match = self.get_argument('match').lower()
        channels = self.db.channels.find()
        if match:
            for channel in channels:
                if match in channel['name'].lower():
                    search_results.append(channel)
        self.render("templates/channel_search.html", channels=search_results)

class WSHandler(tornado.websocket.WebSocketHandler):

    """Basic WebSocket handler.

    Overrides 'open', 'on_message', 'on_close' and 'check_origin' methods
    of base class. Variable 'clients' is a list of all active clients.
    """

    clients = []

    def open(self):
        """If new connection registered, appends it to 'clients' list."""
        self.clients.append(self)
      
    def on_message(self, message):
        """
        If new message received, checks if it's initial message adds new attribute
        'channel'. If it's not initial message sends it to all active clients in 
        current channel.
        """
        message = json.loads(message)
        if message.get('initial', ''):
            self.channel = message['initial']
            return None
        for c in self.clients:
            if c.channel == message['channel']:
                c.write_message("{0} {1}: {2}".format(message['date'], 
                                                      message['user'], 
                                                      message['message'])
                                )
 
    def on_close(self):
        """If connection closed, removes client from list of active clients."""
        self.clients.remove(self)

    def check_origin(self, origin):
        """Tornado origin check.

        Return 'True' to accept all cross-origin traffic.
        """
        return True


def main():
    # Create new tornado web application.
    application = tornado.web.Application(
        [
            (r"/", MainHandler),
            (r"/login", LoginHandler),
            (r"/logout", LogoutHandler),
            (r"/register", RegistrationHandler),
            (r"/register/success", RegisterSuccessHandler),
            (r"/channels/([A-Za-z0-9]+)", ChannelHandler),
            (r"/find/channel", SearchChannelNameHandler),
            (r"/search/([A-Za-z0-9]+)", ChannelSearchHandler),
            (r"/ws", WSHandler),
            (r"/static/(.*)", tornado.web.StaticFileHandler),
        ],
        cookie_secret="dklsjlkjfskdljflkdjsflkdsjlkfs", 
        login_url='/login', 
        static_path=os.path.join(os.path.dirname(__file__), "static")
    )
    # Run HTTP server with created application
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(PORT)
    tornado.ioloop.IOLoop.instance().start()
    
if __name__ == "__main__":
    main()